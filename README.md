Script that allows you to save your Homebrew packages and VSCode extensions in a script (I recommend to add it in your Cron)

In order to use this script, you must have make and curl installed.

This project is meant to be forked without license constraints (public domain), get the sources and have fun with it :)

## To install : 
make install
## To update your system : 
make update 
(these commands can be grouped to be executed manually)