.PHONY: install

install:
	@echo "Installing..."
	curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh | bash
	mkdir -p ~/.saveworksystem
	cp save.sh ~/.saveworksystem/save.sh
	crontab cronconfig

update:
	@echo "Updating..."
	brew update
	brew upgrade
	brew upgrade --cask
	brew doctor
	brew cleanup
	brew missing
	omz update