#!/bin/bash

save_dir=~/.saveworksystem
brew_file=$save_dir/brew.sh
code_file=$save_dir/vscode.sh

if [ ! -d $save_dir ]; then
    mkdir $save_dir
fi

## brew ##

echo "# Formulae:" > $brew_file
for formula in $(/usr/local/bin/brew list --formulae); do
    echo "brew install $formula" >> $brew_file
done

echo "# Casks:" >> $brew_file
for cask in $(/usr/local/bin/brew list --casks); do
    echo "brew install --cask $cask" >> $brew_file
done

echo "# Tap:" >> $brew_file
for tap in $(/usr/local/bin/brew tap); do
    echo "brew tap $tap" >> $brew_file
done

## vscode ##

echo "# Extensions:" > $code_file
for extension in $(/usr/local/bin/code --list-extensions); do
    echo "code --install-extension $extension" >> $code_file
done